![Minimal R Version](https://img.shields.io/badge/R%3E%3D-3.2.0-blue.svg)
![License](https://img.shields.io/badge/License-LGPL-blue.svg)
### Berlanga 2019 + AOU 2020
Se unieron por los nombres en inglés; dos especies no coincidieron por nombre inglés pero sí en latín: _Guatemalan Tyrannulet_ y _Thick-billed Longspur_; una tercera, _Pampa rufa_ carece de clave Avibase.

20 especies no coinciden en nombre científico: 

```r
 setdiff(berlanga$nombre_cientifico, aou$species)
 [1] "Atthis heloisa"            "Atthis ellioti"            "Chlorostilbon auriceps"   
 [4] "Chlorostilbon forficatus"  "Chlorostilbon canivetii"   "Cynanthus sordidus"       
 [7] "Campylopterus curvipennis" "Campylopterus excellens"   "Campylopterus rufus"      
[10] "Thalurania ridgwayi"       "Amazilia candida"          "Amazilia cyanocephala"    
[13] "Amazilia beryllina"        "Amazilia cyanura"          "Amazilia violiceps"       
[16] "Amazilia viridifrons"      "Hylocharis eliciae"        "Hylocharis leucotis"      
[19] "Hylocharis xantusii"       "Cyanolyca nana"
```

### Listados de especies en formato PDF, convertidos a formatos tabulares:
Para quienes hacemos estudios ecológicos, un pdf no es buen formato para revisar cientos de especies, sabrán.
## NOM-059-SEMARNAT 2018 y 2019

Desde el Diario Oficial de la Federación, las NOM-059 de los años [DOF 2018](https://dof.gob.mx/nota_detalle.php?codigo=5534594&fecha=13/08/2018), y [DOF 2019](https://dof.gob.mx/nota_detalle.php?codigo=5578808&fecha=14/11/2019). En el script dentro de este repositorio está como hacer el volcado desde el html al csv.

[Aquí está la NOM-059_2018.csv](https://gitlab.com/datamarindo/listados_tabulares/-/raw/master/tablas_csv_fwf/nom059_2018.csv) 

[Aquí está la NOM-059_2019.csv](https://gitlab.com/datamarindo/listados_tabulares/-/raw/master/tablas_csv_fwf/nom059_2019.csv) 

```r
library(readr)
read_csv("https://gitlab.com/datamarindo/listados_tabulares/-/raw/master/tablas_csv_fwf/nom059_2018.csv")
# A tibble: 2,636 x 11
   familia  genero  especie  categoria_infrae… nombre_infraespe… autor sinonimia      nombres_comunes      distribucion categoria_de_ri… species
   <chr>    <chr>   <chr>    <chr>             <chr>             <chr> <chr>          <chr>                <chr>        <chr>            <chr>  
 1 Agarica… Agaric… augustus                                     Fr.,… Agaricus augu… champiñón de bosque…              A                Agaric…
 2 Amanita… Amanita muscaria                                     (L.)…                hongo de moscas, mo…              A                Amanit…
 3 Bolbiti… Conocy… siligin…                                     R. H…                                     endémica     P                Conocy…
# … with 2,633 more rows
```

_Muchos de los cambios 2018-2019 son solamente actualizaciones taxonómicas de nombres:_
```r

nom059_18 <- read_csv("https://gitlab.com/datamarindo/listados_tabulares/-/raw/master/tablas_csv_fwf/nom059_2018.csv") 
nom059_19 <- read_csv("https://gitlab.com/datamarindo/listados_tabulares/-/raw/master/tablas_csv_fwf/nom059_2019.csv") 
nom059_18_no_19 <- setdiff(nom059_18$species, nom059_19$species) 
nom059_19_no_18 <- setdiff(nom059_19$species, nom059_18$species) 
tibble(anterior = nom059_18_no_19, sinonimia = lapply(nom059_18_no_19, grep, nom059_19$sinonimia, value =T) %>% as.character()) %>%
  left_join(nom059_19[,c(7,11)]) %>% select(-sinonimia, actual_2019 = species) %>%
  filter(!is.na(actual_2019))

# A tibble: 51 x 2
   anterior                 actual_2019               
   <chr>                    <chr>                     
 1 Calibanus hookeri        Beaucarnea hookeri        
 2 Aztekium valdezii        Aztekium ritteri          
 3 Coryphantha melleospina  Coryphantha retusa        
 4 Epiphyllum chrysocardium Selenicereus chrysocardium
 5 Anathallis abbreviata    Lankestariana abbreviata  
 6 Dignathe pygmaeus        Cuitlauzina pygmaea       
# … with 45 more rows
```

## Aves de México (Berlanga et al., 2019)
Este pdf incluye las actualizaciones de AOS, IUCN y NOM-059-SEMARNAT *(A la fecha aproximada de Julio de 2019)*, descargable desde:
https://www.biodiversidad.gob.mx/media/1/ciencia-ciudadana/documentos/Lista_actualizada_aos_2019.pdf; para convertirlo a formato tabular usamos la función pdftotext del paquete poppler-utils (se obtiene de: `sudo apt install poppler-utils`). En bash, con el parámetro fixed, el inicio en página 3 y final en la 18:
[No me importa el código, denme el archivo](https://gitlab.com/datamarindo/listados_tabulares/-/tree/master/tablas_csv_fwf/berlanga_2019n.txt)

```
$ pdftotext -nopgbrk -fixed 6 -f 3 -l 18 Lista_actualizada_aos_2019.pdf berlanga_2019.txt
```

Después quitamos una serie de pelusas con expresiones regulares, puede ser directamente en nuestro editor de texto (obviamente que no en word)

```r
"#.*$"                #nos quita los que empiezan con # Nombre científico...
"^\s*[[:alpha:]].*\n" # quita los órdenes (anseriformes blabla)
"^\s*$"               # quita unos que tienen solo un espacio
"^\n"                 # quita los vacíos
```

Para leerlo en R como ancho fijo de columna (fwf):

```r
nombres = "num,nombre_cientifico,nombre_espanol,nombre_ingles,residencia,nom_059,uicn,end_ex,vv_obs"
read_fwf("https://gitlab.com/datamarindo/listados_tabulares/-/raw/master/tablas_csv_fwf/berlanga_2019n.txt", 
         col_positions = fwf_empty("https://gitlab.com/datamarindo/listados_tabulares/-/raw/master/tablas_csv_fwf/berlanga_2019n.txt")) %>%
  set_names(strsplit(nombres, ",")[[1]])

# A tibble: 1,119 x 9
   Nombre_científico Nombre_espanol           Nombre_ingles         Residencia                   NOM_059 UICN  END_EX VV    OBS  
   <chr>             <chr>                    <chr>                 <chr>                        <chr>   <chr> <chr>  <chr> <chr>
 1 1                 Tinamus major            Tinamú Mayor          Great Tinamou                R       A     NT     ne    13   
 2 2                 Crypturellus soui        Tinamú Menor          Little Tinamou               R       A     LC     ne    11   
 3 3                 Crypturellus cinnamomeus Tinamú Canelo         Thicket Tinamou              R       Pr    LC     ne    14   
# … with 1,116 more rows
```


